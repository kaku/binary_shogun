# vim: noexpandtab tabstop=3 shiftwidth=3 :
CC			= gcc
CFLAGS	= -Isubhook -fPIC
STRIP	  	= sstrip

.DEFAULT_GOAL := bin/shogun.so

bin/:
	mkdir bin

bin/shogun.so: src/shogun.c bin
	$(CC) $(CFLAGS) src/shogun.c -ldl -shared -o bin/shogun.so

release: CFLAGS += -DSHOGUN_RELEASE -g0
release: bin/shogun.so
	$(STRIP) bin/shogun.so

clean:
	-rm bin/shogun.so
