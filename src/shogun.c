#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>

#include <dlfcn.h>
#include <execinfo.h>

#define SUBHOOK_STATIC
#include <subhook.c>

#if defined(SHOGUN_RELEASE)
#   define shogun_log(...) (0)
#   define shogun_backtrace()
#else

static int
shogun_log (char const* fmt, ...)
{
   va_list va;
   int size;

   fprintf(stderr, "Binary Shogun: ");
   va_start(va, fmt);
   size = vfprintf(stderr, fmt, va);
   va_end(va);

   return size;
}

#define shogun_backtrace() \
   { \
      void*  frames[100]; \
      char** frameSyms; \
      int    frameCount; \
      \
      frameCount = backtrace(frames, 100); \
      frameSyms  = backtrace_symbols(frames, frameCount); \
      shogun_log("%s call depth is %i frames:\n", __func__, frameCount); \
      \
      if (frameSyms) \
      { \
         for (int idx = 0; idx < frameCount; ++idx) \
         { \
            shogun_log("%-3i : %s\n", idx, frameSyms[idx]); \
         } \
         free(frameSyms); \
      } \
      else \
      { \
         shogun_log("\tError: could not generate symbolic trace\n"); \
      } \
   }
#endif

char*   BNAllocString(char*);
int64_t BNIsLicenseValidated(); /* .text:0000000000674240 */
static  int64_t/*@rax*/ (*BNQueryLicense)(/* rbp, rdi, r13 */); /* .text:0000000000674250 (BNIsLicenseValidated + 10) */

// BNAreAutoUpdatesEnabled @ libBinaryNinjaCore.so.text:0000000000675E70
extern  void BNAreUpdatesAvailable;
extern  void BNAreAutoUpdatesEnabled;
// ... +32A0 bytes
static  void/*@rax*/ (*BNCheckUpdate)(/* rbp, rdi, r13 */); /* .text:0000000000679110 */
static subhook_t sh_BNCheckUpdate;

int rsa_verify_hash_ex(char*, int64_t, char*, int64_t, int64_t, int64_t, uint64_t, int*, void*);
static subhook_t sh_rsa_verify_hash_ex;

/**
 * rsa signature validation is (a/o 1.0.776) used only in license and update-related tasks
 */
static int32_t
rep_rsa_verify_hash_ex(char* sig,    size_t siglen,
                       char* hash,   size_t hashlen,
                       int padding, int hash_idx, uint64_t saltlen,
                       int* stat, void* key)
{
   shogun_log("rsa_verify_hash_ex()\n");
   shogun_backtrace();
   return 0;
}

/*
 * BNCheckUpdate implementation
 */

/*
 * Replacement for BNCheckUpdate that properly handles stack and side-effects
 * XXX This is definitely a pain point for target updates.
 */
asm (
"rep_BNCheckUpdate:\n"
// Based on BNCheckUpdate+0x0
      "mov   %rdi, %rbx\n"
      "sub   $0x318, %rsp\n" // Account for unneeded (in this case) stack manip (see doc/BNCheckUpdate.s)

// From BNCheckUpdate+0x14 (license failure state)
      // Zero out mystery structure
      "movq  $0, 0x00(%rbx)\n"
      "movq  $0, 0x08(%rbx)\n"
      "movq  $0, 0x10(%rbx)\n"

      "add   $0x318, %rsp\n"
      "retq\n"
);
extern void rep_BNCheckUpdate();


static void __attribute__ ((constructor))
shogun_init (void)
{
   void* dlh = dlopen(NULL, RTLD_LAZY);
   if (dlsym(dlh, "BNIsLicenseValidated") != 0)
   {
      unsetenv("LD_PRELOAD"); // Prevent shogun from breaking children
      shogun_log("Proceeding with attach on BN lib\n");

      BNCheckUpdate         = (&BNAreAutoUpdatesEnabled) + 0x32A0;
      sh_BNCheckUpdate      = subhook_new((void*) BNCheckUpdate,        (void*) rep_BNCheckUpdate,      0);
      sh_rsa_verify_hash_ex = subhook_new((void*) rsa_verify_hash_ex,   (void*) rep_rsa_verify_hash_ex, 0);

      subhook_install(sh_BNCheckUpdate);
      subhook_install(sh_rsa_verify_hash_ex);
   }
   dlclose(dlh);
}
